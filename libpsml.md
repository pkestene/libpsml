---
project: libPSML
summary: Library to handle Pseudopotential Markup Language files
author: Alberto Garcia
project_dir: ./src
             ./examples
page_dir: ./doc/pages
output_dir: /tmp/psml-docs
preprocessor: gfortran -E -P
preprocess: true
exclude: strings_helpers.f90
project_website: http://gitlab.com/siesta-project/libraries/libpsml
email: albertog@icmab.es
extensions: f90
            F90
docmark: +	    
predocmark: >
media_dir: ./doc/media
docmark_alt: *
predocmark_alt: <
source: true
graph: false
search: true
license: bsd
display: public
         private
	 protected
extra_filetypes: sh #
                 inc !
md_extensions: markdown.extensions.toc
---

The library provides an API for parsing PSML files and extracting
information from them.

For more information, and to begin exploring the PSML ecosystem,
check out the [Overview](./page/index.html).
  