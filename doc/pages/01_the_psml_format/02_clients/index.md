title: First-principles codes able to read PSML files

On the client side, we have incorporated the libPSML library in SIESTA
and ABINIT. PSML files can then be directly read and processed by these
codes, achieving pseudopotential interoperability.

### SIESTA

The PSML functionality is implemented in the
[psml-support](https://launchpad.net/siesta/psml-support) series of
development, which will soon be merged into the main version.

### ABINIT

The [Abinit code](https://www.abinit.org) has been able to process PSML files since Version 8.2.
